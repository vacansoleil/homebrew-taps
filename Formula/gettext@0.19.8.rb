class GettextAT0198 < Formula
    desc "GNU internationalization (i18n) and localization (l10n) library"
    homepage "https://www.gnu.org/software/gettext/"
    url "https://ftp.gnu.org/gnu/gettext/gettext-0.19.8.tar.xz"
    mirror "https://ftpmirror.gnu.org/gettext/gettext-0.19.8.tar.xz"
    sha256 "9c1781328238caa1685d7bc7a2e1dcf1c6c134e86b42ed554066734b621bd12f"

    bottle do
      root_url "https://bitbucket.org/vacansoleil/homebrew-taps/downloads/"
      rebuild 1
      sha256 "22570a47aad0e029a3f43d4814a120f9d9de4e99a7c48888e5f6b24655deb034" => :mojave
    end

    def install
      system "./configure", "--disable-dependency-tracking",
                            "--disable-silent-rules",
                            "--disable-debug",
                            "--prefix=#{prefix}",
                            "--with-included-gettext",
                            # Work around a gnulib issue with macOS Catalina
                            "gl_cv_func_ftello_works=yes",
                            "--with-included-glib",
                            "--with-included-libcroco",
                            "--with-included-libunistring",
                            "--with-emacs",
                            "--with-lispdir=#{elisp}",
                            "--disable-java",
                            "--disable-csharp",
                            # Don't use VCS systems to create these archives
                            "--without-git",
                            "--without-cvs",
                            "--without-xz"
      system "make"
      ENV.deparallelize # install doesn't support multiple make jobs
      system "make", "install"
    end

    test do
      system bin/"gettext", "test"
    end
  end

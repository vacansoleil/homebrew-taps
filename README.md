# Vacansoleil Taps

## How do I install these formulae?

`brew tap vacansoleil/taps git@bitbucket.org:vacansoleil/homebrew-taps.git` and then `brew install <formula>`.

## Documentation

`brew help`, `man brew` or check [Homebrew's documentation](https://docs.brew.sh).

